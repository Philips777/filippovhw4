# Есть строка произвольного содержания. Написать код, который найдет в строке самое короткое слово, в котором присутствуют подряд две гласные буквы.

# Любое долгое слово длинее интереснее прошлого
# I love you and see bee agree
My_Input = input("Введите предложение: ")
vowels = ("аеоуиэАЕУОИЭяЯaAeEuUoOyYiI")
for sign in ',аеоуиэАЕУОИЭяЯaAeEuUoOyYiI':
    My_Input = My_Input.replace(sign, '*')
    # print(My_Input)
My_txt = int(My_Input.count('**'))
print(f'Количество гласных слов строке  - {My_txt}')
print(My_txt)
words = My_Input.split()
print(words)

# Есть два числа - минимальная цена и максимальная цена. Дан словарь продавцов и цен на какой то товар у разных продавцов: { "citrus": 47.999, "istudio" 42.999, "moyo": 49.999, "royal-service": 37.245, "buy.ua": 38.324, "g-store": 37.166, "ipartner": 38.988, "sota": 37.720, "rozetka": 38.003}. Написать код, который найдет и выведет на экран список продавцов, чьи цены попадают в диапазон между нижней и верхней ценой. Например:


ppp = {"citrus": 47.999,"istudio":42.999,"moyo": 49.999,"royal-service": 37.245,"buy.ua": 38.324,"g-store": 37.166,"ipartner": 38.988,"sota": 37.720,"rozetka": 38.003}
my_lst = list('ppp')
print(ppp)

_ppp_ = ppp.values()
my_lst1 = str(_ppp_)
print(my_lst1)
print('--->', type(my_lst1))
print(_ppp_)

min = my_lst1[5]
max = my_lst1[2]
for x in my_lst1:
    if x != min or x != max:
        print(x)
